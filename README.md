### Gravahal ###

* implementation of the gravahal game using sparkjava, ektorp, sammy.js
* see below to start
* goto http://localhost:4567/
* make a game
* play!

### How do I get set up? ###

on linux or mac with java 8 and maven installed run:

```sh
cd source/main/resources/public
npm install bower -g
bower install
cd -
./run
```

on windows with java 8 and maven installed:

```sh
cd source\main\resources\public
npm install bower -g
bower install
cd ..\..\..\..\
mvn exec:java -Dexec.mainClass="com.bol.gravahal.App"
```