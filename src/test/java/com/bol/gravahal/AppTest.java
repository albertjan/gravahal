package com.bol.gravahal;

import com.bol.gravahal.model.Game;
import com.bol.gravahal.model.Round;
import java.io.IOException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testShouldBeAbleToDeserializeAGame() throws IOException {
        ObjectMapper om = new ObjectMapper();
        String value = "{ \"currentPlayerToMove\": { \"name\": \"pietje\" }, \"otherPlayer\": { \"name\": \"jantje\" } }";
        Game game = om.readValue(value, Game.class);
        assertEquals(game.getCurrentPlayerToMove().getName(), "pietje");
        assertEquals(game.getOtherPlayer().getName(), "jantje");
    }
    
    public void testShouldBeAbleToDeserializeARound() throws IOException {
        ObjectMapper om = new ObjectMapper();
        String value = "{ \"player\": { \"name\": \"pietje\" }, \"chosenPitNumber\": 3, \"gameId\": \"test\" }";
        Round round = om.readValue(value, Round.class);
        assertEquals(round.getChosenPitNumber(), 3);
        assertEquals(round.getPlayer().getName(), "pietje");
        assertEquals(round.getGameId(), "test");
    }
}