/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal;

import com.bol.gravahal.controllers.GameController;
import com.bol.gravahal.model.GameRepository;
import com.bol.gravahal.model.Game;
import com.bol.gravahal.model.Pit;
import com.bol.gravahal.model.Player;
import com.bol.gravahal.model.Round;
import com.bol.gravahal.model.RoundRepository;
import java.util.Arrays;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import static org.mockito.Mockito.*;
    
/**
 *
 * @author albertjan
 */
public class GameControllerTest 
        extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public GameControllerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( GameControllerTest.class );
    }

    
    public void testGameInitialisation() {
        GameRepository gameRepo = mock(GameRepository.class);
        RoundRepository roundRepo = mock(RoundRepository.class);
        GameController controller = new GameController(gameRepo, roundRepo);
        Game initializedGame = controller.initializeGame(new Game(new Player("test"), new Player("test2")));
        assertEquals(initializedGame.getCurrentPlayerToMove().getName(), "test");
        assertEquals(1, initializedGame.getCurrentPlayerToMove().getPlayerNumber());
        assertEquals(initializedGame.getOtherPlayer().getName(), "test2");
        assertEquals(initializedGame.getCurrentPlayerToMove().getPits().size(), 6);
        assertEquals(initializedGame.getOtherPlayer().getPits().size(), 6);
        assertEquals(initializedGame.getCurrentPlayerToMove(), initializedGame.getCurrentPlayerToMove());
        verify(gameRepo).add(initializedGame);
    }
    
    public void testGamePlayRound1CurrentPlayerGetsToGoAgain() {
        GameRepository gameRepo = mock(GameRepository.class);
        RoundRepository roundRepo = mock(RoundRepository.class);
        GameController controller = new GameController(gameRepo, roundRepo);
        Game initializedGame = controller.initializeGame(new Game(new Player("tets"), new Player("test2")));
        when(gameRepo.get("test_123")).thenReturn(initializedGame);
        Game result = controller.play(new Round(0, 0, new Player("tets"), "test_123"));
        assertEquals(result.getCurrentPlayerToMove().getName(), 
                initializedGame.getCurrentPlayerToMove().getName());
        assertEquals(0, result.getCurrentPlayerToMove().getPits().get(0).getStones());
        assertEquals(1, result.getCurrentPlayerToMove().getGravahal().getStones());
    }
    
    public void testStealingStones() {
        GameRepository gameRepo = mock(GameRepository.class);
        RoundRepository roundRepo = mock(RoundRepository.class);
        GameController controller = new GameController(gameRepo, roundRepo);
        Game initializedGame = controller.initializeGame(new Game(new Player("tets"), new Player("test2")));
        when(gameRepo.get("test_123")).thenReturn(initializedGame);
        Game result = controller.play(new Round(0, 0, new Player("tets"), "test_123"));
        when(gameRepo.get("test_123")).thenReturn(result);
        result = controller.play(new Round(1, 1, new Player("tets"), "test_123"));
        when(gameRepo.get("test_123")).thenReturn(result);
        result = controller.play(new Round(2, 0, new Player("test2"), "test_123"));
        when(gameRepo.get("test_123")).thenReturn(result);
        result = controller.play(new Round(3, 0, new Player("tets"), "test_123"));
        assertEquals(10, result.getOtherPlayer().getGravahal().getStones());
    }
    
    public void testDetermineCorrectlyIfPlayerGetsToPlayAgain () {
        GameRepository gameRepo = mock(GameRepository.class);
        RoundRepository roundRepo = mock(RoundRepository.class);
        GameController controller = new GameController(gameRepo, roundRepo);
        assertTrue(controller.canPlayAgain(6));
        assertTrue(controller.canPlayAgain(19));
        assertTrue(controller.canPlayAgain(32));
        assertTrue(controller.canPlayAgain(45));       
    }
    
    public void testSowStones () {
        GameRepository gameRepo = mock(GameRepository.class);
        RoundRepository roundRepo = mock(RoundRepository.class);
        GameController controller = new GameController(gameRepo, roundRepo);
        
        List<Pit> pits = Arrays.asList(new Pit(0, 6), new Pit(0, 6), new Pit(0, 6),
                new Pit(0, 6), new Pit(0, 2000), new Pit(0, 0), new Pit(0, 6),
                new Pit(0, 6), new Pit(0, 6), new Pit(0, 6), new Pit(0, 6), 
                new Pit(0, 6), new Pit(0, 6));
        
        pits = controller.sowStones(pits, 4);
        assertEquals(159, pits.get(3).getStones());
        assertEquals(153, pits.get(4).getStones());
        assertEquals(154, pits.get(5).getStones());
    }
}
