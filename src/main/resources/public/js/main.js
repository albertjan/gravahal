Sammy("#main", function () {
    this.use('Handlebars', 'hbs');
    
    this.get("/", function () {
        var _this = this;
        $.getJSON("/games").then(function(data) {
            _this.games = data;
            _this.partial("templates/home.hbs");
        });
    });
    
    this.post("#/rounds", function (){
        var _this = this;
        var round = {
            player: {
                name: this.params.player_name
            },
            chosenPitNumber: this.params.chosen_pit,
            gameId: this.params.game_id
        };
        
        $.ajax({
            type: "POST",
            url: "/rounds",
            contentType: "application/json",
            data: JSON.stringify(round)
        }).then(function() {
            _this.redirect("#/games/" + _this.params.game_id);
        }, function (stuff) {
            alert(JSON.stringify(stuff)); 
        });
    });
    
    this.get("#/games/:game_id", function (){
        var _this = this;
        $.getJSON("/games/" + this.params.game_id).then(function(data) {
            _this.game = data;
            _this.player1 = data.currentPlayerToMove.playerNumber === 1 ? data.currentPlayerToMove : data.otherPlayer; 
            _this.player2 = data.currentPlayerToMove.playerNumber === 2 ? data.currentPlayerToMove : data.otherPlayer;
            _this.player1.pits.sort(function(l, r) { return l.number < r.number; });
            _this.player1ToMove = _this.player1.name == data.currentPlayerToMove.name;
            _this.partial("templates/game.hbs");
        });
    });
    
    this.post("#/new_game", function() {
        var _this = this;
        var game = {
            currentPlayerToMove: { name: this.params.player1_name },
            otherPlayer: { name: this.params.player2_name }
        };
        
        $.ajax({
            type: "POST",
            url: "/games",
            data: JSON.stringify(game),
            contentType: "application/json",
            dataType: "json"
        }).then(function(data) {
            _this.redirect("#/games/" + data._id);
        }, function (stuff) {
            alert(JSON.stringify(stuff)); 
        });
    });
    
    this.get("#/new_game", function() {
        this.partial("templates/new_game.hbs");
    });
}).run();
