package com.bol.gravahal;

import com.bol.gravahal.model.RoundRepository;
import com.bol.gravahal.controllers.GameController;
import com.bol.gravahal.model.Game;
import com.bol.gravahal.model.GameRepository;
import com.bol.gravahal.model.Round;
import java.io.IOException;

import static spark.Spark.*;

import spark.ModelAndView;
import spark.template.mustache.MustacheTemplateEngine;

import java.net.MalformedURLException;
import java.util.HashMap;
import org.codehaus.jackson.map.ObjectMapper;

import org.ektorp.*;
import org.ektorp.http.*;
import org.ektorp.impl.*;


/**
 * Hello grava hal!
 *
 */
public class App 
{
    public static void main( String[] args ) throws MalformedURLException, IOException
    {
        staticFileLocation("/public");
        
        // -------- dependency composition --------
        HttpClient httpClient = new StdHttpClient.Builder()
                .url("https://albertjan.cloudant.com/")
                .build();

        CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        CouchDbConnector gameDb = new StdCouchDbConnector("games", dbInstance);
        CouchDbConnector roundDb = new StdCouchDbConnector("rounds", dbInstance);
        GameRepository gameRepository = new GameRepository(gameDb);
        RoundRepository roundRepository = new RoundRepository(roundDb);
        GameController gameController = new GameController(gameRepository, roundRepository);
        ObjectMapper objectMapper = new ObjectMapper();
        IModule<Game> gameModule = new GameModule(gameController, objectMapper);
        IModule<Round> roundModule = new RoundModule(gameController, objectMapper);
        // -----------------------------------------
        
        // index route
        get("/", (request, response) -> {
           return new ModelAndView(new HashMap(), "index.mustache");                
        }, new MustacheTemplateEngine());
    }
}
