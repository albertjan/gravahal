/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal;

import com.bol.gravahal.controllers.GameController;
import com.bol.gravahal.model.Game;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import static spark.Spark.*;

/**
 * GameModule contains the routes for the games resource
 *  
 * @author albertjan
 */
public class GameModule implements IModule<Game> {
    private final GameController controller;
    
    public GameModule(GameController controller, ObjectMapper objectMapper) throws IOException {
        this.controller = controller;
        
        head("/games", (request, response) -> {
            try {
                response.status(200);
                Integer contentLength = 
                        objectMapper.writeValueAsString(controller.getGameRepository().getAll())
                                    .getBytes()
                                    .length;
                response.header("Content-Length", contentLength.toString());
                return "";
            } catch (IOException ex) {
                Logger.getLogger(GameModule.class.getName()).log(Level.SEVERE, null, ex);
                response.status(500);
                return ex.toString();
            }
        });
        
        get("/games", (request, response) -> {
            response.type("application/json");
            return controller.getGameRepository().getAll();
        }, new JsonTransformer());
        
        get("/games/:id", (request, response) -> {
            response.type("application/json");
            return controller.getGameRepository().get(request.params(":id"));
        }, new JsonTransformer());
         
        post("/games", (request, response) -> {
            try {
                Game game = objectMapper.readValue(request.body(), Game.class);
                game = controller.initializeGame(game);
                response.status(201);
                response.header("Location", "/games/" + game.getId());
                return game;
            } catch (IOException ex) {
                Logger.getLogger(GameModule.class.getName()).log(Level.SEVERE, null, ex);
                response.status(500);
                return ex.toString();
            }
        }, new JsonTransformer());
        
        delete("/games/:id", (request, response) -> {
            controller.getGameRepository().remove(controller.getGameRepository().get(request.params(":id")));
            response.status(204);
            return "";
        });
    }
}
