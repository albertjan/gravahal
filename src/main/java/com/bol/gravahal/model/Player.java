/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.model;
import java.util.List;

/**
 *
 * @author albertjan
 */
public class Player {
    
    private String name;
    private List<Pit> pits;
    private Pit gravahal;
    private int playerNumber;
    
    public Player() {
    }
    
    public Player(String name) {
        this.name = name;
    }
    
    public Player(String name, int number) {
        this.name = name;
        this.playerNumber = number;
    }
    
    public Player(String name, int number, List<Pit> pits, Pit gravahal) {
        this.name = name;
        this.playerNumber = number;
        this.pits = pits;
        this.gravahal = gravahal;
    }
    
    public String getName() {
        return this.name;
    }
    
    public List<Pit> getPits() {
        return this.pits;
    }
    
    public Pit getGravahal() {
        return this.gravahal;
    }

    public int getPlayerNumber() {
        return this.playerNumber;
    }
}