/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.model;

import java.util.List;
import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.ektorp.support.CouchDbRepositorySupport;

/**
 *
 * @author albertjan
 */
public class RoundRepository extends CouchDbRepositorySupport<Round> {
    public RoundRepository(CouchDbConnector db) {
        super(Round.class, db);
    }
    
    public List<Round> getRoundsPerGame(String gameid) {
        ViewQuery query = new ViewQuery()
                     .designDocId("_design/Round")
                     .viewName("by_game")
                     .key(gameid);
                
        return db.queryView(query, Round.class);
    }
}
