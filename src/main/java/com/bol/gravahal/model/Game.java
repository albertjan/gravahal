/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author albertjan
 */
@JsonIgnoreProperties({ "id", "revision" })
public class Game {
    
    public Game() {
    }
    
    public Game(Player current, Player other) {
        this.currentPlayerToMove = current;
        this.otherPlayer = other;
    }
    
    public Game(Player current, Player other, boolean currentPlayerHasWon) {
        this.currentPlayerToMove = current;
        this.otherPlayer = other ;
        this.hasEnded = true;
        this.playerThatWon = current;
    }
    
    @JsonProperty("_id")
    private String id;

    @JsonProperty("_rev")
    private String revision;
    
    private Player otherPlayer;
    private Player currentPlayerToMove;
    
    private boolean hasEnded;
    private Player playerThatWon;
    
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getRevision() {
        return revision;
    }
    
    public void setRevision(String revision) {
        this.revision = revision;
    }
    
    public Player getOtherPlayer() {
        return otherPlayer;
    }
    
    public Player getCurrentPlayerToMove() {
        return currentPlayerToMove;
    }
    
    public Player getPlayerThatWon(){
        return playerThatWon;
    }
    
    public boolean getHasEnded() {
        return hasEnded;
    }
}
