/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.model;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;

/**
 *
 * @author albertjan
 */
public class GameRepository extends CouchDbRepositorySupport<Game> {
    public GameRepository(CouchDbConnector db) {
        super(Game.class, db);
    }
}
