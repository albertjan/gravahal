/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author albertjan
 */
@JsonIgnoreProperties({ "id", "revision" })
public class Round {

    @JsonProperty("_id")
    private String id;

    @JsonProperty("_rev")
    private String revision;
    
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getRevision() {
        return revision;
    }
    
    public void setRevision(String revision) {
        this.revision = revision;
    }
    
    public Round() {
    }
    
    public Round(int number, int chosenPit, Player player, String gameId) {
        this.roundNumber = number;
        this.chosenPitNumber = chosenPit;
        this.player = player;
        this.gameId = gameId;
    }
        
    private int roundNumber;
    private int chosenPitNumber;
    private Player player;
    private String gameId;
    
    public int getRoundNumber() {
        return this.roundNumber;
    }
    
    public int getChosenPitNumber () {
        return this.chosenPitNumber;
    }
    
    public Player getPlayer () {
        return this.player;
    }
    
    public String getGameId () {
        return this.gameId;
    }
}
