/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.model;

/**
 *
 * @author albertjan
 */
public class Pit {
    public Pit() {
    }
    
    public Pit(int number, int stones) {
        this.number = number;
        this.stones = stones;
    }
    
    private int number;
    private int stones;
    
    public int getStones() {
        return stones;
    }
    
    public int getNumber() {
        return number;
    }
    
    public Pit increase() {
        return new Pit(number, stones + 1);
    }

    public Pit empty() {
        return new Pit(number, 0);
    }
}