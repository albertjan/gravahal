/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal.controllers;

import com.bol.gravahal.model.Game;
import com.bol.gravahal.model.Pit;
import com.bol.gravahal.model.Player;
import com.bol.gravahal.model.Round;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ektorp.support.CouchDbRepositorySupport;

/**
 * Game controller contains the logic to modify the game state.
 * 
 * @author albertjan
 */
public class GameController {
    private final CouchDbRepositorySupport<Game> gameRepository;
    private final CouchDbRepositorySupport<Round> roundRepository;
    
    /**
     * Initializes a new game controller use this to alter the state of games
     * @param gameRepository database repository to get games
     * @param roundRepository database repository for rounds
     */
    public GameController(CouchDbRepositorySupport<Game> gameRepository, 
            CouchDbRepositorySupport<Round> roundRepository) {
        this.gameRepository = gameRepository;
        this.roundRepository = roundRepository;
    }
    
    /**
     * Get the game repository
     * 
     * @return the game repository
     */
    public CouchDbRepositorySupport<Game> getGameRepository() {
        return this.gameRepository;
    }

    /**
     * Get the round repository
     * 
     * @return the round repository
     */
    public CouchDbRepositorySupport<Round> getRoundRepository() {
        return this.roundRepository;
    }
    
    /**
     * Initialize a new game, from a game that just has the player names
     * 
     * @param game with just player names
     * @return the initialized game
     */
    public Game initializeGame(Game game) {
        Player player1 = initialzePlayer(game.getCurrentPlayerToMove(), 1);
        Player player2 = initialzePlayer(game.getOtherPlayer(), 2);
        Game newGame = new Game(player1, player2);
        this.gameRepository.add(newGame);
        return newGame;
    }

    /**
     * Gathers the pits that are relevant to this round
     *  
     * @param game
     * @return a list of pits
     */
    public List<Pit> gatherPits(Game game) {
        List<Pit> pits = new ArrayList<>();
        pits.addAll(game.getCurrentPlayerToMove().getPits());
        pits.add(game.getCurrentPlayerToMove().getGravahal());
        pits.addAll(game.getOtherPlayer().getPits());
        return pits;
    }
    
    /**
     * Sow stones in pits keeps looping until all the stones have been sowed
     * 
     * @param pits to sow the stones in
     * @param chosenPit the pit to get the stones to sow from
     * @return the pits but with the stones redistributed
     */
    public List<Pit> sowStones(List<Pit> pits, int chosenPit) {
        int stonesToSow = pits.get(chosenPit).getStones();
        pits.set(chosenPit, pits.get(chosenPit).empty());
        int currentPit;
        
        for (int i = 1; i <= stonesToSow; i++) {
            currentPit = ((chosenPit + i) % pits.size());
            pits.set(currentPit, pits.get(currentPit).increase());
        }
        
        return pits;
    }
    
    /**
     * Determines whether or not the game ended
     * @param pits to see if someone won
     * @return true when the game ended
     */
    public boolean didGameEnd(List<Pit> pits) {
        return pits.subList(0, 6).stream().mapToInt(Pit::getStones).sum() == 0 ||
               pits.subList(7, pits.size()).stream().mapToInt(Pit::getStones).sum() == 0;
    }
    
    /**
     * empties pits from index upto another index
     * @param pits the pits
     * @param start where to start emptying (including)
     * @param end where to stop emptying (excluding)
     * @return the list of pits with the emptied pits
     */
    public List<Pit> emptyPits(List<Pit> pits, int start, int end) {
        for (int i = start; i < end; i++) {
            pits.set(i, pits.get(i).empty());
        }
        return pits;
    }
    
    /**
     * Get all stones from list and add the stones from the gravahal
     * @param pits the pits to sum
     * @param gravahal the gravahal
     * @return the number of stones in all the pits
     */
    public int getTotalStones(List<Pit> pits, Pit gravahal){
        return pits.stream().mapToInt(Pit::getStones).sum() + 
                gravahal.getStones();
    }
    
    /**
     * Determines whether of not the currentPlayer gets to play again
     * @param lastStoneIndex the index of the pit where the last stone ended up
     * @return true when the current player can play again
     */
    public boolean canPlayAgain(int lastStoneIndex) {
        return (lastStoneIndex - 6) % 13 == 0;
    }

    /**
     * Tries to steal stones from the other player when the last stone lands in
     * an empty it takes the stones from both pits and puts them into the current
     * players gravahal
     * @param lastStoneIndex the index the last stone landed
     * @param pits the pits 
     * @return the pits with the stones redistributed
     */
    public List<Pit> tryStealStones(int lastStoneIndex, List<Pit> pits) {
        lastStoneIndex = lastStoneIndex % 13;
        boolean landsInOwnPit = lastStoneIndex < 6;
        int oppositePitIndex = 12 - (lastStoneIndex);
        int numberOfStonesInOppositePit = pits.get(oppositePitIndex).getStones();
        if (landsInOwnPit && pits.get(lastStoneIndex).getStones() == 1) {
            pits.set(oppositePitIndex, pits.get(oppositePitIndex).empty());
            pits.set(lastStoneIndex, pits.get(lastStoneIndex).empty());
            pits.set(6, new Pit(-1, pits.get(6).getStones() + numberOfStonesInOppositePit + 1));
        }
        
        return pits;
    }    
    
    /**
     * Initializes a player with game start values
     * 
     * @param player1 player to initialize
     * @param number player number
     * @return an initialized player
     */
    public Player initialzePlayer(Player player1, int number) {
        Pit gravahal = new Pit(-1, 0);
        List<Pit> pits = Arrays.asList(new Pit(0, 6), new Pit(1, 6), 
                new Pit(2, 6), new Pit(3 ,6), new Pit(4, 6), new Pit(5, 6));
        return new Player(player1.getName(), number, pits, gravahal);
    }
    
    /**
     * Play a round in the game, this does the following things:
     *  - Gets the current state;
     *  - Saves rev and id because we need them to update the game in the db;
     *  - Gathers the pits according to who played;
     *  - sows the stones;
     *  - tries to steal stones;
     *  - see if the game ended;
     *  - creates a new game with the new state;
     *  - adds the rev and id back in
     * 
     * @param round Round that will be played
     * @return the new game state
     */
    public Game play(Round round) { 
        Game game = this.gameRepository.get(round.getGameId());
        String gameId = game.getId();
        String rev = game.getRevision();
        
        List<Pit> pits = gatherPits(game);
        int stonesToSow = pits.get(round.getChosenPitNumber()).getStones();
        
        pits = sowStones(pits, round.getChosenPitNumber());
        pits = tryStealStones(round.getChosenPitNumber() + stonesToSow, pits);
        boolean gameHasEnded = didGameEnd(pits);
        int otherPlayersStones = 
                getTotalStones(pits.subList(7, 13), game.getOtherPlayer().getGravahal());
        int currentPlayerStones = 
                getTotalStones(pits.subList(0, 6), pits.get(6));
        
        if (gameHasEnded) { 
            pits = emptyPits(pits, 0, 6);
            pits = emptyPits(pits, 7, 13);
        }
 
        Player other = 
                new Player(
                        game.getOtherPlayer().getName(),
                        game.getOtherPlayer().getPlayerNumber(),
                        pits.subList(7, pits.size()),
                        gameHasEnded ? new Pit(-1, otherPlayersStones) : game.getOtherPlayer().getGravahal()
                );
        
        Player current = 
                new Player(
                        game.getCurrentPlayerToMove().getName(),
                        game.getCurrentPlayerToMove().getPlayerNumber(),
                        pits.subList(0, 6),
                        gameHasEnded ? new Pit(-1, currentPlayerStones) : pits.get(6)
                    );
        
        
        if (gameHasEnded) {
            boolean currentPlayerIsWinner = currentPlayerStones > otherPlayersStones;
            game = new Game(currentPlayerIsWinner ? current : other, currentPlayerIsWinner ? other : current, gameHasEnded);
        } else {
            game = canPlayAgain(round.getChosenPitNumber() + stonesToSow) ? 
                new Game(current, other) : 
                new Game(other, current);
        }
        
        game.setId(gameId);
        game.setRevision(rev);
        return game;
    }
}
