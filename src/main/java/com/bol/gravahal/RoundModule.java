/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal;

import com.bol.gravahal.controllers.*;
import com.bol.gravahal.model.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import static spark.Spark.*;

/**
 * Round module contains the routes for the rounds resource
 * 
 * @author albertjan
 */
public class RoundModule implements IModule<Round> {
    public RoundModule (GameController gameController, ObjectMapper objectMapper) {
        get("/games/:gameid/rounds", (req, resp) -> {
            resp.type("application/json");
            return ((RoundRepository)gameController.getRoundRepository()).getRoundsPerGame(req.params(":gameid"));
        }, new JsonTransformer());
        
        get("/rounds/:roundid", (req, resp) -> {
            resp.type("application/json");
            return gameController.getRoundRepository().get(req.params(":roundid"));
        }, new JsonTransformer());
        
        post("/rounds", (req, resp) -> {
            try {
                Round round = objectMapper.readValue(req.body(), Round.class);
                Game game = gameController.play(round);
                gameController.getRoundRepository().add(round);
                gameController.getGameRepository().update(game);
                resp.status(201);
                return "";
            } catch (Exception ex) {
                Logger.getLogger(RoundModule.class.getName()).log(Level.SEVERE, null, ex);
                resp.status(500);
                return ex.toString();
            }
        });
    }
}
