/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bol.gravahal;

import spark.ResponseTransformer;
import org.codehaus.jackson.map.ObjectMapper;
/**
 *
 * @author albertjan
 */
public class JsonTransformer implements ResponseTransformer {

    ObjectMapper om = new ObjectMapper();
    
    public JsonTransformer() {
    }

    @Override
    public String render(Object o) throws Exception {
        return om.writeValueAsString(o);
    }
}
